nums = []
while True:
    user_num = input("Please Enter a Number: ")
    if user_num == "":
        break

    try:
        nums.append(float(user_num))
    except:
        print(f"{user_num} is not a number!")

if len(nums) == 0:
    print("No numbers were given.")

else:
    total = 0
    for n in nums:
        total += n
    
    average = total / len(nums)

    print(f"Total of all numbers: {total}")
    print(f"Average of all numbers: {average}")


        
    

